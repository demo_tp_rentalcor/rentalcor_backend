# Trabajo Práctico IAEW

## Technologies:
- Node
- Mongo
- Mongoose
- Express
- Swagger Open Api 3.0
- Docker
- Docker Compose
  
## How to run:
- `npm install`
- `npm run start`

## Local port:

- 3000

## Swagger API's definition:

- `/api/swagger`
