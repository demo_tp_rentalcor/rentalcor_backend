const mongoose = require('mongoose')
const { Schema } = mongoose

const Reservation = new Schema({
    code: Number,
    date: Date,
    userId: String,
    cost: Number,
    salePrice: Number
})

module.exports = mongoose.model('Reservation', Reservation)