const mongoose = require('mongoose')
const { Schema } = mongoose

const Quote = new Schema({
    codigoReserva: String,
    fechaReserva: String,
    usuarioReserva: String,
    totalReserva: Number,
    pasajero: Object,
    fechaHoraRetiro: String,
    fechaHoraDevolucion: String,
    estado: Number,
    fechaCancelacion: String,
    usuarioCancelacion: Object,
    ciudad: Object,
    vehiculo: Object
})

module.exports = mongoose.model('Quote', Quote)