const mongoose = require('mongoose')
const { Schema } = mongoose

const Vehicle = new Schema({
    name: String,
    description: String
})

module.exports = mongoose.model('Vehicle', Vehicle)