const mongoose = require('mongoose')
const { Schema } = mongoose

const Location = new Schema({
    country: String,
    province: String
})

module.exports = mongoose.model('Location', Location)