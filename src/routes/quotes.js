const express = require('express')
const router = express.Router()
const axios = require('axios');
const auth = require('../Authenticator')
const Quote = require('../models/Quote')

// Get all quotes
router.get('/', async (req, res) => {
    const quotes = await Quote.find()
    if (quotes) res.json({ status: 200, quotes: quotes })
    res.json({ status: 404, msg: 'Quotes not found' })
})

// Make a new quote
router.post('/', auth.getTicket, (req, res) => {
    let quoteInfo = {};
    axios.get('https://uoi1izahv5.execute-api.us-east-1.amazonaws.com/Prod/vehiculos/cotizar?idCiudad=' + req.body.provinceId + '&fechaHoraRetiro=' + req.body.dateFrom + '&fechaHoraDevolucion=' + req.body.dateUntil, { headers: { authorization: `${req.ticket.token_type} ${req.ticket.access_token}` } })
        .then(respuesta => {
            if (respuesta.data && respuesta.status === 200) {
                res.json({ status: 200, quoteInfo: respuesta.data })
            } else res.json({ status: 500, message: 'Error' })
        })
})

router.post('/confirmQuote', auth.getTicket, (req, res) => {
    axios.post('https://uoi1izahv5.execute-api.us-east-1.amazonaws.com/Prod/reservas', req.body, { headers: { authorization: `${req.ticket.token_type} ${req.ticket.access_token}` } })
        .then(respuesta => {
            if (respuesta.data && respuesta.status === 201) {
                const quote = new Quote(respuesta.data)
                quote.save()
                res.json({ status: 201, message: 'Quote created.', quote: quote })
            }
        })
})

router.post('/cancel', auth.getTicket, (req, res) => {
    console.log('llegue a metodo de back y cod reserva tiene: ')
    axios.delete('https://uoi1izahv5.execute-api.us-east-1.amazonaws.com/Prod/reservas/' + req.body.codigoReserva, { headers: { authorization: `${req.ticket.token_type} ${req.ticket.access_token}` } })
        .then(respuesta => {
            console.log('llegue antes del if')
            res.json({ status: 200, message: 'Reservation deleteds.' })
        })
})

router.post('/update/:quoteId', async (req, res) => {
    console.log('llegue a update: ', req.params.quoteId)
    await Quote.findByIdAndUpdate(req.params.quoteId, { estado: 2 })
    res.json({ status: 200, message: 'Reservation modified.' })
})

module.exports = router