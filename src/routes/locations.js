const express = require('express')
const router = express.Router()
const axios = require('axios');
const auth = require('../Authenticator')

// Get all Locations
router.get('/', auth.getTicket, (req, res) => {
    let countries = [];
    axios.get("https://uoi1izahv5.execute-api.us-east-1.amazonaws.com/Prod/paises", { headers: { authorization: `${req.ticket.token_type} ${req.ticket.access_token}` } })
        .then(response => {
            response.data.forEach(loc => countries.push(loc))
            res.json({ status: 200, countries: countries })
        })

})

router.post('/country/provinces', auth.getTicket, (req, res) => {
    let provinceNames = [];
    let provinces = [];
    let countryId = req.body.countryId;
    axios.get("https://uoi1izahv5.execute-api.us-east-1.amazonaws.com/Prod/paises/" + countryId + "/ciudades", { headers: { authorization: `${req.ticket.token_type} ${req.ticket.access_token}` }})
        .then(respuesta => {
            respuesta.data.forEach(province => {
                provinces.push(province);
                provinceNames.push(province.nombre);
            });
            res.json({ status: 200, provinces: provinces, provinceNames: provinceNames })
        });
})



module.exports = router