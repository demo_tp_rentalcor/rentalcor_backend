/* const express = require('express')
const router = express.Router()
const Reservation = require('../models/Reservation')

// Get all Reservations
router.get('/', async (req, res) => {
    const reservations = await Reservation.find()
    res.json({ status: 200, reservations: reservations })
})
// Get an especific Reservation
router.get('/:_id', async (req, res) => {
    const reservation = await Reservation.findById(req.params._id)
    if (!reservation) res.json({ status: 400, message: 'Reservation not found.' })
    res.json({ status: 200, reservation: reservation })
})
// Save a new Reservation
router.post('/', async (req, res) => {
    const reservation = new Reservation(req.body)
    await reservation.save()
    res.json({ status: 201, message: 'Reservation created.' })

})
// Edit an existing Reservation
router.put('/:_id', async (req, res) => {
    await Reservation.findByIdAndUpdate(req.params._id, req.body)
    res.json({ status: 200, message: 'Reservation modified.' }
    )
})
// Delete a Reservation
router.delete('/:_id', async (req, res) => {
    await Reservation.findByIdAndDelete(req.params._id)
    res.json({ status: 200, message: 'Reservation deleted.' }
    )
})

module.exports = router */