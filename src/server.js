const express = require('express')
const morgan = require('morgan')
const mongoose = require('mongoose')
const swaggerUI = require('swagger-ui-express')
const swaggerDocument = require('./swagger')
const config = require('./config');

// Definitions
const app = express()
/* dbURI Local uncomment this to run local mongo instance: */
/* const dbURI = `mongodb://localhost:${config.MONGO_LOCAL_PORT}/admin`; */
const dbURI = `mongodb://tpuser:tppass@${config.MONGO_DNS}:27017/admin`
const mongooseOptions = { useNewUrlParser: true, auto_reconnect: true }
const db = mongoose.connection;
require('events').EventEmitter.prototype._maxListeners = 0;

// // Allow CORS
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
    next();
});

// Database events
db.on('error', function (error) {
    mongoose.disconnect();
});
db.on('connected', function () {
    console.log('MongoDB connected!');
});
db.once('open', function () {
    console.log('MongoDB connection opened!');
});
db.on('reconnected', function () {
    console.log('MongoDB reconnected!');
});
db.on('disconnected', function () {
    console.log('MongoDB disconnected!');
    mongoose.connect(dbURI, mongooseOptions);
});

mongoose.connect(dbURI, mongooseOptions);

// Server setup
app.use(morgan('dev'))
app.use(express.json())
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

// Server routing
/* app.use('/api/vehicles', require('./routes/vehicles')) */
/* app.use('/api/reservations', require('./routes/reservations')) */
app.use('/api/quotes', require('./routes/quotes'))
app.use('/api/locations', require('./routes/locations'))
app.use(express.static(__dirname + '/public'))
app.use('/api/swagger', swaggerUI.serve, swaggerUI.setup(swaggerDocument))


// Server start
app.listen(config.PORT, config.HOST);