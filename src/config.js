// Variables de entorno
module.exports = {
    MONGO_DNS: process.env.MONGO_DNS || 'localhost', 
    MONGO_LOCAL_PORT: 27016,
    PORT: process.env.PORT || 3000,
    HOST:'0.0.0.0'
  };